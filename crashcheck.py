# inclue lib
import os
import re
#import ssl
import sys
#import shutil
import psutil
import chardet
import datetime
import requests
import colorama
import subprocess
import webbrowser
#os.remove('crashcheck.exe')
#=================================================================================

"""
## 自验证证书添加
# 安装OpenSSL后写入：
openssl genrsa -out private.key 2048
openssl req -new -key private.key -out certificate.csr
openssl x509 -req -in certificate.csr -signkey private.key -out certificate.crt
# 加载自签名证书文件
certfile = 'path/to/certificate.crt'
keyfile = 'path/to/private.key'
context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
context.load_cert_chain(certfile=certfile, keyfile=keyfile)
# 在网络请求或服务器设置中使用上述的 context
# 例如，使用 Flask 框架的示例：
from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello():
    return 'Hello, world!'

if __name__ == '__main__':
    app.run(ssl_context=context)
"""

## 全局版本号
Global_version_SOFT = "0.46beta" #本地
Global_version_GitLab = "0" #待获取

## 获取执行路径
executable_file = sys.argv[0] or os.path.abspath(__file__)
#print(executable_file) 

# 删除原有的肯德基文件
if os.path.exists('crash_temp_kfc756_the_king_of_shit'):
    os.remove('crash_temp_kfc756_the_king_of_shit')

## 获取当前进程名
def get_current_process_name():
    process = psutil.Process()
    return process.name()
if __name__ == "__main__":
    current_process_name = get_current_process_name()
    #print("Current process name:", current_process_name) #crashcheck.exe

#=================================================================================

# check ANSI support function
def supports_ansi():
    if os.name == "nt":
        # Windows
        try:
            colorama.init()
            return True
        except:
            return False
    else:
        # not Windows
        term_look = os.environ.get("TERM")
        return term_look and "256color" in term_look.lower()

# check ANSI support or not
if supports_ansi():
    ANSI_support = True
    #print("ANSI 支持")
else:
    ANSI_support = False
    #print("ANSI 不支持")


#=================================================================================

network_error_occur = None
## 获取远端版本号
# version define
local_version_SOFT = Global_version_SOFT

# url define
version_url = "https://gitlab.com/LeftOwlRight/crashlogchecker/-/raw/main/ver.json?ref_type=heads"
if ANSI_support:
    print("[\033[32m更新\033[0m] 检查更新中...（该流程最多花费\033[34;1m1\033[0m分钟）")
else:
    print("[更新] 检查更新中...（该流程最多花费1分钟）")

try:
    # send HTTP request for JSON data
    response = requests.get(version_url, timeout = 10)
    response.raise_for_status()  # check if succeed

    # decode JSON data
    json_data = response.json()
    remote_version_GitLab = json_data[0]["version"]
    Global_version_GitLab = remote_version_GitLab

    # print("Version:", remote_version_GitLab) # print version
except requests.exceptions.Timeout:
    if ANSI_support:
        print("\033[33m网络连接超时\033[0m")
    else:
        print("网络连接超时")
    network_error_occur = True
except requests.exceptions.RequestException as e:
    if ANSI_support:
        print("[\033[31m错误\033[0m] 发生错误")
        print("\033[33m失败\033[0m：", str(e))
    else:
        print("[错误] 发生错误")
        print("更新检查失败：", str(e))
    network_error_occur = True


#=================================================================================


## 检测目录文件
# 路径和名字
#~~current_exe_dir = os.path.dirname(sys.executable)
#~~detect_filename = "PD2-CrashChecker.exe"
# 拼接文件的完整路径
#~~detect_file_path = os.path.join(current_exe_dir, detect_filename)
# 检测文件是否存在
# 这里会有一个问题，如果本地文件落后于远程文件，但二者文件名相同，会导致程序认为本地版本已更新，而继续使用本地落后文件
#~~if os.path.isfile(detect_file_path):
#~~    Update_File_Exist = True
#~~else:
#~~    Update_File_Exist = False


#=================================================================================

update_file = None
Update_Downloaded = None
## 下载更新
if not network_error_occur and local_version_SOFT != Global_version_GitLab:#~~ and not Update_File_Exist:
    if ANSI_support:
        print("\033[32m发现新版本，开始更新...\033[0m")
    else:
        print("发现新版本，开始更新...")
    # 下载更新文件
    update_download_url = "https://gitlab.com/LeftOwlRight/crashlogchecker/-/raw/main/crashcheck.exe?ref_type=heads&inline=false"
    try:
        response = requests.get(update_download_url, timeout = 50)
        response.raise_for_status()

        # 这里改名字是为了防止远程文件与本地文件重名带来的冲突，同时也方便更新之后删除旧文件
        os.rename(os.path.basename(sys.argv[0]), 'crash_temp_kfc756_the_king_of_shit')

        # 将更新文件保存到本地
        update_file = "PD2-CrashChecker_" + str(Global_version_GitLab) + ".exe"
        with open(update_file, "wb") as f:
            f.write(response.content)

        Update_Downloaded = True
    except requests.exceptions.Timeout:
        if ANSI_support:
            print("\033[33m网络连接超时\033[0m")
        else:
            print("网络连接超时")
        network_error_occur = True
    except requests.exceptions.RequestException as ec:
        if ANSI_support:
            print("[\033[31m错误\033[0m] 无法下载更新文件")
            print("\033[33m失败\033[0m：", str(ec))
        else:
            print("[错误] 无法下载更新文件")
            print("失败：", str(ec))
        network_error_occur = True

else:
    # no need to update
    if not network_error_occur:
        if ANSI_support:
            print("\033[32m已是最新版本！\033[0m")
        else:
            print("已是最新版本！")
        

Run_New = None
if Update_Downloaded:
    if ANSI_support:
        print("\033[32m成功\033[0m：更新完毕！运行新版程序[\033[93m"+str(update_file)+"\033[0m]...")
    else:
        print("成功：更新完毕！运行新版程序["+str(update_file)+"]...")
    # 运行更新程序
    try:
        # 这里必须使用Popen才能保证删除旧文件，使用call会导致父进程杀不死从而无法删除旧文件
        subprocess.Popen([update_file])
        Run_New = True
    except:
        if ANSI_support:
            print("[\033[31m错误\033[0m] 执行新版程序出错")
            print("[\033[33m警告\033[0m] 将运行旧版程序")
            print("[\033[33m提示\033[0m]：如果是杀毒软件拦截，允许执行后新版程序依然下载成功，只需添加信任后手动运行新版程序即可。")
        else:
            print("[错误] 执行新版程序出错")
            print("[警告] 将运行旧版程序")
            print("提示：如果是杀毒软件拦截，允许执行后新版程序依然下载成功，只需添加信任后手动运行新版程序即可。")
    if Run_New:
        sys.exit(0)


if network_error_occur:
    if ANSI_support:
        print("[\033[33m警告\033[0m] 更新失败")
    else:
        print("[警告] 更新失败")
        
        
print("="*100)

#=================================================================================

if ANSI_support:
    print('\033[1m软件版本：'+str(Global_version_SOFT)+'\033[0m')
else:
    print('软件版本：'+str(Global_version_SOFT))


error_occur = None

## 访问闪退日志
# access crash.txt
AppPath = os.getenv('localappdata')
AppData = ''

txt_data_encoding_detect = None
txt_data_encoding_detect_result = None
txt_data_encoding_detect_encoding = None

decoding_way = None

try:
    with open(AppPath + '\PAYDAY 2\crash.txt', 'rb') as file:
        txt_data_encoding_detect = file.read()
        txt_data_encoding_detect_result = chardet.detect(txt_data_encoding_detect)
        txt_data_encoding_detect_encoding = txt_data_encoding_detect_result['encoding']
except Exception as error_when_getting_encoding:
    if ANSI_support:
        print("[{}错误{}] 日志读取失败或不存在".format('\033[31m', '\033[0m'))
    else:
        print("[错误] 日志读取失败或不存在")
        #print(str(error_when_getting_encoding))
        error_occur = True

try:
    with open(AppPath+'\PAYDAY 2\crash.txt', 'r', encoding='utf-8') as file:
        AppData = file.read().replace('/', '\\')
        AppData_Frontline = ''.join(file.readlines()[:4]).replace('/', '\\')
        decoding_way = 'utf-8'
except Exception as error_when_decoding_utf8:
    #print(str(error_when_decoding_utf8))
    try:
        with open(AppPath+'\PAYDAY 2\crash.txt', 'r', encoding='utf-8-sig') as file:
            AppData = file.read().replace('/', '\\')
            AppData_Frontline = ''.join(file.readlines()[:4]).replace('/', '\\')
            decoding_way = 'utf-8-sig'
    except Exception as error_when_decoding_utf8sig:
        #print(str(error_when_decoding_utf8sig))
        try:
            with open(AppPath+'\PAYDAY 2\crash.txt', 'r', encoding='gbk') as file:
                AppData = file.read().replace('/', '\\')
                AppData_Frontline = ''.join(file.readlines()[:4]).replace('/', '\\')
                decoding_way = "gbk"
        except Exception as error_when_decoding_gbk:
            #print(str(error_when_decoding_gbk))
            try:
                with open(AppPath+'\PAYDAY 2\crash.txt', 'r', encoding=txt_data_encoding_detect_encoding) as file:
                    AppData = file.read().replace('/', '\\')
                    AppData_Frontline = ''.join(file.readlines()[:4]).replace('/', '\\')
                    decoding_way = txt_data_encoding_detect_encoding
            except Exception as error_when_decoding:
                if ANSI_support:
                    print("[{}错误{}] 日志读取失败：".format('\033[31m', '\033[0m')+str(error_when_decoding))
                else:
                    print("[错误] 日志读取失败："+str(error_when_decoding))
                #print(str(error_when_decoding))
                error_occur = True


#=================================================================================


## 获取文件修改时间
# get file modified time
file_path_datetime = os.path.join(AppPath, 'PAYDAY 2', 'crash.txt')


if os.path.exists(file_path_datetime):
    modification_time = os.path.getmtime(file_path_datetime)
    modification_datetime = datetime.datetime.fromtimestamp(modification_time)
    #print('')
    if not error_occur:
        print("闪退报告的生成时间：", modification_datetime)
        print("请检查生成时间是否符合闪退发生的时间\n   ——若不符合，则你的问题没有闪退报告生成，请向相关社区求助")
    else:
        print("文件读取失败，但是获取了闪退报告的生成时间：", modification_datetime)
else:
    if ANSI_support:
        print("{}失败{}：文件不存在\n\n".format('\033[33m', '\033[0m'))
    else:
        print("失败：文件不存在\n\n")
    error_occur = True



if error_occur:
    input("按下回车键退出...")
    sys.exit(1)


#=================================================================================



## 这个函数要用r()！
def Pattern_All(text):
    Pattern = re.compile(text)
    return Pattern.findall(AppData)

def Script_Small_or_Big():
    if Pattern_All(r'Script stack'):
        return 'small'
    elif Pattern_All(r'SCRIPT STACK'):
        return 'big'
    else:
        return None
    
Script_Letter = Script_Small_or_Big()


## 这个函数要用r()！
def Current_Pattern(regular_letter, text_pfunc):
    if not text_pfunc:
        text_pfunc = AppData
    pattern = re.compile(regular_letter)
    content_exist = pattern.search(text_pfunc)
    return content_exist #not str  #content_exist[0] ≈ content 

## 这个函数要用r()！
def Current_Pattern_group(regular_letter, text_pfunc):
    if not text_pfunc:
        text_pfunc = AppData
    content_exist = Current_Pattern(regular_letter, text_pfunc)
    if content_exist:
        content = content_exist.group()
        #print(content)
        return content #str  #content ≈ content_exist[0] 
    

## 下面的函数都不要用r()

regular_Application = r'(?<=Application has crashed:)[\s\S]*?(?=--------------------)'
regular_Application_SS_exist = r'(?<=Application has crashed:)[\s\S]*?(?=SCRIPT)'
text_of_Application = Current_Pattern_group(regular_Application, None)
text_of_Application_SS_exist = Current_Pattern_group(regular_Application_SS_exist, None)
if text_of_Application_SS_exist:
    if len(text_of_Application) > len(text_of_Application_SS_exist):
        text_of_Application = text_of_Application_SS_exist
def Pattern_Application(text):
    content = text_of_Application
    if content:
        if text in content:
            return text
        
regular_Callstack = r'(?<=Callstack:)[\s\S]*?(?=--------------------)'
text_of_Callstack = Current_Pattern_group(regular_Callstack, None)
def Pattern_Callstack(text):
    content = text_of_Callstack
    if content:
        if text in content:
            return text
        
regular_Currentthread = r'(?<=Current thread:)[\s\S]*?(?=--------------------)'
text_of_Currentthread = Current_Pattern_group(regular_Currentthread, None)
def Pattern_Currentthread(text):
    content = text_of_Currentthread
    if content:
        if text in content:
            return text

regular_Scriptstack_big = r'(?<=SCRIPT STACK)[\s\S]*?(?=--------------------)'
regular_Scriptstack_big_underline = r'(?<=SCRIPT_STACK)[\s\S]*?(?=--------------------)'
regular_Scriptstack_small = r'(?<=Script stack)[\s\S]*?(?=--------------------)'
def Pattern_Scriptstack(text):
    pattern_big = re.compile(regular_Scriptstack_big) # re.compile(r'(?<=SCRIPT STACK)[\s\S]*(?=-------------------------------\s*Callstack:)')
    pattern_big_underline = re.compile(regular_Scriptstack_big_underline)
    pattern_small = re.compile(regular_Scriptstack_small) # re.compile(r'(?<=Script stack)[\s\S]*(?=-------------------------------\s*Callstack:)')
    content_exist = pattern_big.search(AppData)
    if content_exist:
        #print('BIG')
        content = content_exist.group()
        if text in content:
            return text, content
        else:
            return None, content
    else:
        content_exist = pattern_small.search(AppData)
        if content_exist:
            #print('Small')
            content = content_exist.group()
            if text in content:
                return text, content
            else:
                return None, content
        else:
            #print('underline')
            content_exist = pattern_big_underline.search(AppData)
            if content_exist:
                content = content_exist.group()
                if text in content:
                    return text, content
                else:
                    return None, content
            else:
                return None, None
        
array = Pattern_Scriptstack('array')
text_of_Scriptstack = array[1]  # could be None, remember to check by using 'if'


#~~regular_global_nil = r'(?<=attempt to index global)[\s\S]*?(?=\(a nil value\)'
            
#================================================================================

CPU_SSE_support1 = Pattern_All(r'(SSE4.1)')
CPU_SSE_support2 = Pattern_All(r'(SSE4.2)')

if not error_occur:
    if not CPU_SSE_support1 and not CPU_SSE_support2:
        print(str(decoding_way) + ":[您的CPU不支持SSE4.1和SSE4.2指令集，很可能无法使用Modern CPU Support (2021+)启动游戏]")
    elif not CPU_SSE_support1:
        print(str(decoding_way) + ":[您的CPU不支持SSE4.1指令集，可能无法使用Modern CPU Support (2021+)启动游戏]")
    elif not CPU_SSE_support2:
        print(str(decoding_way) + ":[您的CPU不支持SSE4.2指令集，极小可能无法使用Modern CPU Support (2021+)启动游戏]")
    else:
        print(str(decoding_way) + ":[您的CPU支持SSE4.1和SSE4.2指令集]")

print("-"*100)

if not error_occur:
    if ANSI_support:
        print("\033[1m日志自动分析如下：\033[0m")
    else:
        print("日志自动分析如下：")

##test
#Pattern_Application('d')
#Pattern_Callstack('d')
#Pattern_Currentthread('d')

Daring_New_Check = None
Daring_Not_Included = None


savefile_location = r"游戏存档位置：steam根目录/userdata/你的好友码/218620/remote/save098.sav"

Cheater_log = Pattern_All(r'(train)')
Cheater_CarryStacker = Pattern_All(r'(CarryStacker)')
Cheater_LockSmasher = Pattern_All(r'(LockSmasher)')

Unknown_Thread = Pattern_Currentthread('<Unknown>')

Mining_Video_Card_P = Pattern_All(r'(NVIDIA P)')
Mining_Video_Card_CMP = Pattern_All(r'(NVIDIA CMP)')

payday2_win32_release_callstack = Pattern_Callstack('payday2_win32_release')

PatternModName = re.compile(r'mods\\(.*?)\\')
PatternLuaName_ori = re.compile(r'\\.*\\(.*?)"')
PatternLuaName = re.compile(r'\\.*\\(.*?):')
Err = Current_Pattern(r': attempt to index global \'(.*)\'', text_of_Application)

ModName = PatternModName.findall(text_of_Application)
LuaName = PatternLuaName_ori.findall(text_of_Application)
if not LuaName:
    LuaName = PatternLuaName.findall(text_of_Application)
if ModName:
    PatternMod_p1 = re.compile(r'(No Mutants Allowed)')
    Mod_p1 = PatternMod_p1.findall(AppData)

    PatternMod_p2 = re.compile(r'(Hotline-Miami)')
    Mod_p2 = PatternMod_p2.findall(AppData)


PatternAcc = re.compile(r'(access violation)')
Acc_Exist = PatternAcc.findall(AppData)

privileged_instruction = Pattern_All(r'privileged instruction')

PatternScriptStack = re.compile(r'(Script stack)')
ScriptStack_Exist = PatternScriptStack.findall(AppData)

PatternUpdate = re.compile(r'(update)')
Update = PatternUpdate.findall(AppData)

Pattern_upd_actions = re.compile(r'(_upd_actions)')
upd_actions = Pattern_upd_actions.findall(AppData)

PatternCopactionhurt = re.compile(r'(copactionhurt)')
Copactionhurt = PatternCopactionhurt.findall(AppData)

PatternCRF = re.compile(r'(Cannot read from file)')
CRF = PatternCRF.findall(AppData)



PatternCop = re.compile(r'(cop)')
Cop = PatternCop.findall(AppData)

Cop_Application = Pattern_Application('cop')
bwe_weapon_usage_tweak = Pattern_Application('weapon_usage_tweak')

sufficient_memory = Pattern_Application('sufficient memory')

PatternTex = re.compile(r'(not load texture)')
Tex = PatternTex.findall(AppData)

index_a_number_value = Pattern_All(r'(attempt to index a number value)')

hudchat = Pattern_Application('hudchat')
local_scroll_up_indicator_shade = Pattern_Application('scroll_up_indicator_shade')

create_statics_unit = Pattern_Scriptstack('_create_statics_unit')[0]
coreworlddefinition = Pattern_Scriptstack('coreworlddefinition')[0]
WorldDefinition_load = Pattern_Scriptstack('WorldDefinition_load')[0]
WorldDefinition_package = Pattern_Scriptstack('init_package')[0]

Restoration_App = Pattern_Application('estoration')
Restoration_last_App = Pattern_Application('ation-mod')
Restoration_content_App = Pattern_Application('lua/sc')
weaponfacttory_tweakdata_App = Pattern_Application('weaponfactorytweakdata')
RestorationMod_part_data = Pattern_Application('part_data')
require_equal_C = Pattern_All(r'(require\(\) =\[C\])') #require_equal_C = Pattern_All(r'require\(\) =\[C\]')

xaudio_App = Pattern_Application('xaudio')
OutOfMemory = Pattern_Application('OutOfMemory')

Renderer_Thread = Pattern_Currentthread('Render')

occlusionmanager_App = Pattern_Application('occlusionmanager')
local_obj_App = Pattern_Application("attempt to index local 'obj'")

playerstandard_App = Pattern_Application('playerstandard')
setting_tap_to_interact = Pattern_Application('setting_tap_to_interact')

field_nav_path = Pattern_Application("attempt to index field '_nav_path'")


PatternLib = re.compile(r'\[string (.*)\]')
Lib = PatternLib.findall(AppData)
if Lib:
    PatternLib_p1 = re.compile(r'(explosionmanager)')
    Lib_p1 = PatternLib_p1.findall(Lib[0])

    PatternLib_p2 = re.compile(r'(blackmarketgui)')
    Lib_p2 = PatternLib_p2.findall(Lib[0])

    PatternLib_p3 = re.compile(r'(blackmarketmanager)')
    Lib_p3 = PatternLib_p3.findall(Lib[0])
    if Lib_p3:
        PatternLib_p3_1 = re.compile(r'(table)')
        Lib_p3_1 = PatternLib_p3_1.findall(AppData)

        PatternLib_p3_2 = re.compile(r'(skin)')
        Lib_p3_2 = PatternLib_p3_2.findall(AppData)

    PatternCore = re.compile(r'coreunitdamage')
    Core = PatternCore.findall(Lib[0])

    PatternLib_p4 = re.compile(r'(criminalsmanager)')
    Lib_p4 = PatternLib_p4.findall(Lib[0])

    PatternLib_p5 = re.compile(r'(doctorbagbase)')
    Lib_p5 = PatternLib_p5.findall(Lib[0])

    PatternLib_p6 = re.compile(r'(contourext)')
    Lib_p6 = PatternLib_p6.findall(Lib[0])

    PatternLib_p7 = re.compile(r'(mousepointermanager)')
    Lib_p7 = PatternLib_p7.findall(Lib[0])

    PatternLib_p8 = re.compile(r'(newraycastweaponbase)')
    Lib_p8 = PatternLib_p8.findall(Lib[0])

    PatternLib_p9 = re.compile(r'(playermanager)')
    Lib_p9 = PatternLib_p9.findall(Lib[0])

    PatternLib_p10 = re.compile(r'(basenetworkhandler)')
    Lib_p10 = PatternLib_p10.findall(Lib[0])

    PatternLib_p11 = re.compile(r'(sentrygunweapon)')
    Lib_p11 = PatternLib_p11.findall(Lib[0])

    PatternLib_p12 = re.compile(r'(unitnetworkhandler)')
    Lib_p12 = PatternLib_p12.findall(Lib[0])


match = False
has_Cheated = None

# no cheating
if Cheater_log or Cheater_CarryStacker or Cheater_LockSmasher:
    if ANSI_support:
        print("[\033[91m警告\033[0m] 检测到你使用了外挂/脚本，本程序将不予提供帮助。")
    else:
        print('检测到你使用了外挂/脚本，本程序将不予提供帮助。')
    has_Cheated = True
    match = True

if has_Cheated:
    input("按下回车键退出...")
    sys.exit(2)


if Renderer_Thread and not sufficient_memory:
    print('检测到问题与显示渲染方面有关：'
          '\n   ——如果你是在启动游戏后闪一下黑屏就闪退，那么一般是没有打开3D加速导致，使用DirectX修复工具打开3D加速即可。'
          '\n   ——如果不是上述原因，那么仅凭闪退报告也无法判断问题所在，不过通常不会反复发生，可以直接无视。')
    match = True


if occlusionmanager_App and local_obj_App:
    print('检测到这是一个偶然的游戏问题，尝试安装下面这个模组修复：'
          '\n   自动打开链接：https://modworkshop.net/mod/31684')
    webbrowser.open('https://modworkshop.net/mod/31684')
    match = True

if bwe_weapon_usage_tweak and Cop_Application:
    print('检测到该闪退是由于旧版本的Bot Weapon and Equipment这一模组导致的，请更新。')
    match = True

if index_a_number_value and not text_of_Scriptstack:
    print('检测到这是一个游戏问题，通常不会反复发生，无视即可。')
    match = True

# Mod
if ModName and LuaName and not CRF:
    print('您的模组[' + ModName[0] + ']在代码文件的"' + LuaName[0] + '"上出现了问题，请检查这个模组')
    if Mod_p1:
        print('检测到可能是NoMA出现问题\n在确保你使用最新版本的NoMA并使用了更新工具的前提下，'
              '该闪退可能是由于你使用了某个mod'
              '大幅度更改或重做了敌人的伤害及其相关机制导致NoMA无法正常检测。')

    if Mod_p2:
        print('检测到可能是因为火线迈阿密(Hotline-Miami)HUD改名导致的问题')
    if Err:
        var_global_nil = Current_Pattern_group(r"'([^']*)'", Err[0])
        print('模组在' + var_global_nil + '上存在空值')
    if (Restoration_App or Restoration_last_App or Restoration_content_App) and (RestorationMod_part_data or require_equal_C) and weaponfacttory_tweakdata_App:
        print('检测到你未安装最新版本的Beardlib/SuperBLT，或Beardlib/SuperBLT的文件异常，因此无法启动恢复Mod。\n——请安装或重新安装Beardlib和SuperBLT')
        match = True
    #print('Vinight提示：若删除模组依然出现闪退，那么请验证游戏文件完整性')
    #match = True

skilltree_savefile_broken_lua = Pattern_Application('skilltreemanager')
skilltree_savefile_broken_data = Pattern_Application("'skill_data'")
skilltree_savefile_broken_script = Pattern_Scriptstack('savefilemanager')[0]

if skilltree_savefile_broken_lua and skilltree_savefile_broken_data and skilltree_savefile_broken_script:
    print('检测到你存档中，装备库的预设或技能树的预设损坏：')
    print('尝试装回更多装备库预设及更多技能树预设的模组，若无效，则手动删档处理（可能需要关闭云存档）。')
    print(savefile_location)
    match = True

if playerstandard_App and setting_tap_to_interact:
    print('检测到可能是一戴面具就会发生的闪退，在游戏选项中恢复默认设置即可。')
    match = True

Blackmarket = Pattern_Application('blackmarketmanager')

if Blackmarket:
    print('检测到你的装备库存档可能损坏，请尝试安装如下mod修复：')
    print("自动打开链接：https://pan.baidu.com/s/15Hxa5iw6mNdOoWT1HgtJZg?pwd=eeee 提取码：eeee\n")
    webbrowser.open('https://pan.baidu.com/s/15Hxa5iw6mNdOoWT1HgtJZg?pwd=eeee')
    Daring_New_Check = True
    match = True

if field_nav_path:
    print('这是一个确定存在于游戏版本1.143.243的问题')
    print('安装下面这个模组修复（The Fixes）：'
          '\n自动打开链接：https://gitlab.com/LeftOwlRight/the-fixes')
    webbrowser.open('https://gitlab.com/LeftOwlRight/the-fixes')
    Daring_New_Check = True
    match = True

if Acc_Exist and Update and upd_actions and Copactionhurt:
    print('这是一个确定存在于游戏版本1.143.243的问题')
    print('安装下面这个模组修复（Tased Domination Crash Bandaid）：'
          '\n自动打开链接：https://modworkshop.net/mod/46649')
    webbrowser.open('https://modworkshop.net/mod/46649')
    match = True

##判断可能有问题？
if not payday2_win32_release_callstack:
    print('这是一个无法准确定位问题所在的闪退')
    print('这通常是游戏外部问题，如系统、驱动等方面的问题。'
          '\n     ——首先建议在相关社区提问，但由于这类问题比较复杂且可能无法确定原因，所以难以解决，你不一定能得到完美的解决方案。'
          '\n     ——作为最后的解决方案，重装较新版本的系统、更换硬件等通常可以有效解决问题。')
elif ((Acc_Exist and not ScriptStack_Exist) or privileged_instruction or ((create_statics_unit or WorldDefinition_load or WorldDefinition_package) and coreworlddefinition)) and not CRF:
    print('这是一个无法准确定位问题所在的闪退')
    print('这种情况通常只是游戏问题，可能的原因及解决方法如下：'
          '\n     内存溢出，32位程序最多使用4GB内存（带有音频、图片、或模型文件的模组会提高内存使用）'
          '\n     ——尝试在视频设置中将纹理质量调低一档。')
    match = True
elif Acc_Exist:
    print('这是一个无法准确定位问题所在的闪退')
    if ANSI_support:
        print('通过查看完整的日志，\033[4m检查Script stack中前两行的内容是否指向任意模组\033[0m'
            '\n     ——如果\033[1m是\033[0m，尝试\033[1m删除对应的模组\033[0m'
            '\n     ——如果前两行没有指向任意模组：'
            '\n       1.检查你新安装或者是新更新的模组'
            '\n       2.请直接备份模组文件，然后检查游戏文件完整性'
            '\n       3.使用二分法排查模组')
    else:
        print('通过查看完整的日志，检查Script stack中前两行的内容是否指向任意模组'
            '\n     ——如果是，尝试删除对应的模组'
            '\n     ——如果前两行没有指向任意模组：'
            '\n       1.检查你新安装或者是新更新的模组'
            '\n       2.请直接备份模组文件，然后检查游戏文件完整性'
            '\n       3.使用二分法排查模组')

    

game_base_init_package_missing = Pattern_Currentthread('package')

if game_base_init_package_missing:
    print('检测到游戏文件缺失或异常，检查游戏缓存完整性通常就可以解决。\n——若依然未解决问题，尝试重装游戏（不推荐）')
    match = True

if Unknown_Thread and (Mining_Video_Card_CMP or Mining_Video_Card_P):
    print('检测到你的显卡为矿卡，即虚拟货币运算专用显卡。\n这类显卡对游戏的支持较差，如果你打不开游戏，建议你更换显卡或寻找支持游戏运行的方式（如尝试使用其它驱动版本）。\n')
    match = True


if CRF:
    print('因不能读取文件而导致闪退，通常是如下两种原因之一：'
          '\n     1.内存溢出，32位程序最多使用4GB内存（带有音频、图片、或模型文件的模组会提高内存使用）'
          '\n       ——尝试在视频设置中将纹理质量调低一档，这通常可以解决或改善这一问题。'
          '\n     2.游戏文件缺失或异常，删除对应文件后检查游戏缓存完整（概率较低）')
    match = True

    if Cop:
        print('您的报错似乎提示警察在两个动作之间有异常移动，这属于游戏引擎问题。')
    match = True

if hudchat and local_scroll_up_indicator_shade:
    print('检测到这是你安装的某个Hud模组引起的问题，请自行排查，然后更新或删除这个模组。')
    Daring_New_Check = True
    match = True

if xaudio_App and OutOfMemory:
    print('检测到您的游戏在加载第三方音轨时因内存溢出而闪退，\n32位程序最多使用4GB内存（带有音频、图片、或模型文件的模组会提高内存使用。')
    print('——您可以通过降低纹理质量或降低音频文件大小来改善或延迟这一问题。无论如何，播放过多的第三方音频最终总会导致内存溢出。')
    match = True

if Lib and not Daring_New_Check:
    print('您的游戏的模组或者是游戏本身在使用'+Lib[0]+'这个接口时出现问题，请检查是否有模组使用这个函数')
    print('检查你安装的模组的mod.txt里是否引用此接口\n')
    Daring_Not_Included = True
    if Lib_p1:
        print('检测到您这次的报错基本上属于游戏内出现问题，重新上线游玩即可')
        Daring_Not_Included = None

    if Lib_p2:
        print('检测到您这次的报错可能属于存档问题，请恢复备份存档')
        Daring_Not_Included = None

    if Lib_p3:
        if Lib_p3_1:
            print('检测到您这次的报错可能属于更多预设的模组导致的')
        elif Lib_p3_2:
            print('检测到您这次的报错可能由第三方武器或皮肤引起\n请检查自己的皮肤、面具、放置物模组')
        else:
            print('检测到您这次的报错可能属于错误删除第三方武器导致的存档损坏，使用模组修复')
            print('模组链接：\n'
                  '自动打开链接：https://pan.baidu.com/s/15Hxa5iw6mNdOoWT1HgtJZg?pwd=eeee 提取码：eeee')
            webbrowser.open('https://pan.baidu.com/s/15Hxa5iw6mNdOoWT1HgtJZg?pwd=eeee')
        Daring_Not_Included = None

    if Lib_p4:
        print('检测到您这次的报错可能属于某个受到攻击的单位无法受到爆炸伤害\n可能是如下模组导致：\n1.无友军伤害')
        Daring_Not_Included = None

    if Lib_p5:
        print('检测到您这次的报错可能属于网络太差，丢包导致的数据异常闪退')
        Daring_Not_Included = None

    if Lib_p6:
        print('检测到您这次的报错可能属于FSS和IO(输入输出流)引起的冲突')
        Daring_Not_Included = None

    if Lib_p7:
        print('检测到您这次的报错可能属于淘宝买了全dlc作弊后删档所致\n温馨提示：浪子回头金不换')
        Daring_Not_Included = None

    if Lib_p8:
        print('检测到您这次的报错可能属于第三方武器残留，自行寻找并删除残留数据的武器')
        Daring_Not_Included = None

    if Lib_p9:
        print('检测到您这次的报错可能属于:'
              '\n     1.主副手通用的模组造成的存档数据异常(损坏),报错为:963'
              '\n     2.主武器异常，这是由于你曾经使用第三方武器或武器配件后，在不使用或删除模组时未能完全清除第三方武器数据造成的,报错为:965'
              '\n     3.副武器异常，这是由于你曾经使用第三方武器或武器配件后，在不使用或删除模组时未能完全清除第三方武器数据造成的,报错为:966'
              '\n     4.强制友谊代码异常，报错为：281 修复链接：https://pan.baidu.com/s/1AiYLLywqqIlNy3rr6SPXvQ 提取码：chin')
        webbrowser.open('https://pan.baidu.com/s/1AiYLLywqqIlNy3rr6SPXvQ')
        Daring_Not_Included = None

    if Lib_p10:
        print('检测到您这次的报错可能属于HUD或者插件版本太老，请更新HUD(更有可能是pocohud和大胡子(Beardlib)')
        Daring_Not_Included = None

    if Lib_p11:
        print('检测到您这次的报错可能属于网络太差在同步炮台子弹时出错(传输数据包出错)')
        Daring_Not_Included = None

    if Lib_p11:
        print('检测到您这次的报错可能属于自己或某个玩家安装了一个自动设置炮台为AP模式的模组，自动设置的信息接收早于放置的信息导致的。\n'
              '也可能是正常玩家网络延迟过高，给房主发送了错乱的信息导致的非恶意性炸房。')
        Daring_Not_Included = None

    if Core:
        print('检测到您这次的报错可能属于典型的游戏本身问题，电脑因为某种原因承载不起当前状态的计算导致闪退')
    match = True

if Tex or sufficient_memory:
    print('\n检测到内存溢出，32位程序最多使用4GB内存（带有音频、图片、或模型文件的模组会提高内存使用）'
          '\n    ——尝试在视频设置中将纹理质量调低一档，这通常可以解决或改善这一问题。')
    match = True


if not match or Daring_Not_Included:
    print('未收录此类报错\n如有需要，请在相关社区中提问')

print('-'*100)

if ANSI_support:
    print("提示:\n本程序进行的自动检测分析仅供参考，若程序分析未能解决问题，请\033[31m查看完整报告\033[0m并截图发在贴吧或群聊提问。\n\n检查完整的错误报告请输入Y，并按下回车")
else:
    print("提示:\n本程序进行的自动检测分析仅供参考，若程序分析未能解决问题，请查看完整报告并截图发在贴吧或群聊提问。\n\n检查完整的错误报告请输入Y，并按下回车")

inputStr = input()
if inputStr == 'y' or inputStr == 'Y':
    print(AppData)
    print('软件版本'+str(Global_version_SOFT)+'，按回车结束软件')
    print('软件制作：Vinight QQ:1121169088')
    print('软件更新：LR_Daring & Vi.night')
    print('闪退日志及解决方法提供：LR_Daring')
    inputStr = input()

# pyinstaller --onefile crashcheck.py
# pyinstaller -i f.ico -F 查看闪退日志.py
